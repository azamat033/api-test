<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index($id)
    {
//        return Task::find($id);
        return Task::with("category")->find($id);
    }

    public function getCategory($id)
    {
        return Task::find($id)->category;
    }

    public function setStatus($id)
    {
        $status = request('status');
        $model = Task::find($id);
        $model->status = !$status;
        $res = $model->save();
        if ($res)
        {
            return $model;
        }
    }
}
