<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $timestamps = false;
    public function category()
    {
        return $this->hasOne(Category::class, "id", "category_id");
    }
}
