<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/category', 'CategoryController@index');
Route::get('/task/{id}', 'TaskController@index');
Route::put('/task/setStatus/{id}', 'TaskController@setStatus');
Route::get('/category/{id}', 'CategoryController@getTasks');
